# barrage-vue


## 如果项目帮助到了您，请记得点个start哦  ^_^



## 演示地址
http://barrage.houry.top


## v2.x版本功能

* 敏感词汇过滤
* 断网重连
* 设置弹幕颜色
* 标识自己发送的弹幕
* 弹幕位置固定功能
* 历史弹幕列表
* 历史观看总次数展示
* 标题展示
* 音量调节
* 总弹幕展示
* 实时在线观看人数展示
* 弹幕位置固定功能

### 版本介绍
后端基于springboot-2.4.3、redis、netty、protocol buffer。

前端基于vue webSocket 这里参考了弹幕的样式和组件 参考了vue-danmu的开源代码，再次表示感谢。

### 特别说明
1.此版本为前后段分离的项目，后端代码第一阶段已经完毕。目前项目就本人自己维护，有很多还不成熟的地方，欢迎大家集思广益共同维护！

2.项目会一直维护下去，不定时更新。

3.目前1.0版本和2.0版本 已经发布。 2.0版本采用前后端分离的框架进行设计。

### 后端地址
java： https://gitee.com/MonkeyBrothers/barrage

go:    https://gitee.com/MonkeyBrothers/barrage-go

### 安装教程
```
npm install

npm run serve
```

### 历史版本

[前后端分离版本]-[V2.0版本] https://gitee.com/MonkeyBrothers/barrage-vue/tree/V2.0/

### V2.1.0版本效果截图
![avatar](/images/v2.1.0/1.png)
![avatar](/images/v2.1.0/2.png)
![avatar](/images/v2.1.0/3.png)

### V2.0版本效果截图
![avatar](/images/v2.0/1.png)
![avatar](/images/v2.0/2.png)
![avatar](/images/v2.0/3.png)
![avatar](/images/v2.0/4.png)


### V1.0版本效果截图
![avatar](/images/v1.0/1.png)
![avatar](/images/v1.0/2.png)
![avatar](/images/v1.0/3.png)

### 关注我
![avatar](/images/v1.0/WeChat.png)
