const wsUri = "ws://localhost:9999/ws";
const wsBinaryType = "arraybuffer";
const videoId = "1294384753222123";
const userId = "98321837583332345";

const config = {
    wsUri:wsUri,
    wsBinaryType:wsBinaryType,
    videoId:videoId,
    userId:userId,
}

export default config;